

class Personne:
    def __init__(self, nom, age):
        self.nom = nom
        self.age = age


class Voiture:
    def __init__(self, modele, prix, proprietaire):
        self.modele = modele
        self.prix = prix
        self.proprietaire = proprietaire


p = Personne("CookieHacker", 22)

v1 = Voiture("Peugeot 306 1996", 1500, p)
v2 = Voiture("Citroene C4", 15000, p)

print((v1.modele, v1.prix, v1.proprietaire.nom))
print((v2.modele, v2.prix, v2.proprietaire.nom))
